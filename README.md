# batchMerge
## Description
batchMerge merges paired-end fastq files dataset-specifically.
To ensure safe and correct file merging, the consistency of
all file pairs is checked of a dataset based on:
*  file name (sample number, lane number, strand number, tile number)
*  read counts

Additionally, potential collisions between output files and existing files is checked.
For each dataset, a log with detailed information about the merge process is created. 
Optionally, source files can be deleted automatically after successful merge.

If installed, batchMerge takes advantage of the high performant `cat` and `zcat`/`gunzip`
commands.

## Prerequisites
All files to be merged must be gz-compressed (indicated by a gz or gzip extension).
File naming has to follow the pattern

`<dataset_name>_S<sample_number>_L<3digit-lane-number>_R<strand_number>_<3digit-tile-number>.<fq_or_fastq>.<gz_or_gzip>`

*e.g. 190715_19-04970_17-00215_S2_L002_R2_001.fastq.gz*

**System requirements:**
*  Python 3
*  pandas 0.23 or higher
*  recommended: cat, zcat/gunzip

## How to use
```bash
#lazy (merge all gz compressed files in the current working directory and store merged files in the 'merged' directory)
batchMerge.py -o merged *.gz

#thoroughful (as lazy but use the prefix 'merged_data_' for merged files)
batchMerge.py -o merged -p merged_data_ *.gz

#brave (as lazy but overwrite automatically if output files already exist)
batchMerge.py -o merged --force *.gz

#stingy (as lazy but delete source files after successful merge)
batchMerge.py -o merged --remove *.gz

````



