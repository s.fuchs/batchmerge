#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author: Stephan Fuchs (Robert Koch Institute, FG13, fuchss@rki.de)

__version__ = "0.0.11"

import os
import argparse
import re
import sys
import pandas as pd 
import gzip
import datetime
import subprocess
import sys
import shutil

class fqmerger():
	def __init__(self, *fnames, outdir=None, prefix='', force=False, remove=False, quiet=False):
		'''
		provide a list of gzip-compressed paired fastq files for merging
		the filename has to match to the following regex
		(.*)_(S\d+)_(L\d{3})_(R[12])_(\d{3})\.((?:fastq|fq)(?:\.gz))$
		
		optional:
		outdir	output directory path (must exist)
		prefix  prefix string used for output file names
		force   if true, force file overwriting
		remove  if true, remove source files if succesfully merged
		quiet   if true, no messages to stdout
		
		'''
		
		self.prefix = prefix
		self.force = force
		self.quiet = quiet
		self.remove = remove
		if not outdir or os.path.isdir(outdir):
			self.outdir = outdir
		else:
			sys.exit("Error: '" + outdir + "' is not a valid or existing directory.")
		self._wc = self.check_wc()
		self._cat = self.check_cat()
		self._zcat = self.check_zcat()
		self._gunzip = self.check_gunzip()
		df_cols = [
		'basename',
		'absname',
		'label',
		'sample',
		'lane',
		'strand',
		'tile',
		'ext',
		'reads',
		]
		self.df = pd.DataFrame(columns=df_cols)
		self._grouped_df = None
		self.fname_pattern = re.compile(r'(.*)_(S\d+)_(L\d{3})_(R[12])_(\d{3})\.((?:fastq|fq)(?:\.gz))$')
		self.excluded = set()
		
		self.add_files(*fnames)
	
	@property
	def grouped_df(self):
		if not self._grouped_df:
			self._grouped_df = self.df.sort_values(['label', 'strand', 'sample', 'lane', 'tile']).groupby(['label'])		
		return self._grouped_df
			
	
	@staticmethod
	def check_zcat():	
		try:
			p = subprocess.check_output(('zcat', '-h'))
			return True
		except:
			return False
			
	@staticmethod
	def check_wc():	
		try:
			p = subprocess.check_output(('wc', '--help'))
			return True
		except:
			return False			
			
	@staticmethod
	def check_gunzip():	
		try:
			p = subprocess.check_output(('gzip', '-h'))
			return True
		except:
			return False
	
	@staticmethod
	def check_cat():	
		try:
			p = subprocess.check_output(('cat', '--help'))
			return True
		except:
			return False			
			
	def add_files(self, *fnames):
		rows = []
		for fname in fnames:
			if not os.path.isfile(fname):
				print("Error: file '" + fname + "' not found", file=sys.stderr)
				sys.exit()			
			
			bname = os.path.basename(fname)
			match = self.fname_pattern.match(fname)
			
			if not match:
				print("Warning: file '" + fname + "' does not match file name criteria and has been skipped.", file=sys.stderr)
				continue
				
			absname = os.path.abspath(fname)
			
			if absname not in self.df['absname']:
				rows.append({
					'basename': bname,
					'absname': absname,
					'label': match.group(1),
					'sample': match.group(2),
					'lane': match.group(3),
					'strand': match.group(4),
					'tile': match.group(5),
					'ext': match.group(6),
					'reads': None
				})
				
		if rows:
			self.df = self.df.append(rows, ignore_index=True, sort=False)
			self.df['id'] = self.df.index
			self._grouped_df = None
			self.excluded = set()
			
	def name_log(self, label):
		label = self.prefix + label
		if self.outdir:
			label = os.path.join(self.outdir, label)
		return os.path.abspath(label + ".log")
		
	def name_fq(self, label, ext="fastq.gz"):
		label = self.prefix + label
		if self.outdir:
			label = os.path.abspath(os.path.join(self.outdir, label))
		return [label + "_S001_L001_R1_001." + ext, label + "_S001_L001_R2_001." + ext]
	
	@staticmethod
	def is_gz(fname):
		if fname.endswith("gz") or fname.endswith("gzip"):
			return True
		else:
			return False

	@staticmethod			
	def format_table(*rows, algn=None, na="-", delim="   "):
		'''
		returns a formated table
		'''
		formatted_rows = []
		formatted_cols = []
		for j, cols in enumerate(zip(*rows)):
			cols = [na if not x else str(x) for x in cols ]
			formatted_cols.append([])
			maxl = max([len(x) for x in cols])
			for i, col in enumerate(cols):
				spacer = " " * (maxl-len(col))
				if not algn or algn[j] == "l":
					col = col + spacer
				else:
					col = spacer + col
				formatted_cols[-1].append(col)
					
		for cols in zip(*formatted_cols):
			formatted_rows.append(delim.join(cols))
				
		return formatted_rows
		
	def count_reads(self, fname):
		if self.is_gz(fname):
			if self._zcat and self._wc:
				p1 = subprocess.Popen(('zcat', fname), stdout=subprocess.PIPE)
				assert p1.returncode is None
				p2 = subprocess.Popen(('wc', '-l'), stdin=p1.stdout, stdout=subprocess.PIPE)
				assert p2.returncode is None
				lines = int(p2.stdout.read().decode(sys.stdout.encoding).strip())
			elif self._gunzip and self._wc:
				p1 = subprocess.Popen(('gunzip', '-c', fname), stdout=subprocess.PIPE)
				assert p1.returncode is None
				p2 = subprocess.Popen(('wc', '-l'), stdin=p1.stdout, stdout=subprocess.PIPE)
				assert p2.returncode is None
				lines = int(p2.stdout.read().decode(sys.stdout.encoding).strip())
			else:
				with gzip.open(fname, "rt") as handle:
					lines = 0
					for line in handle:
						lines += 1
		else:
			if self._cat and self._wc:
				p1 = subprocess.Popen(('cat', fname), stdout=subprocess.PIPE)
				assert p1.returncode is None
				p2 = subprocess.Popen(('wc', '-l'), stdin=p1.stdout, stdout=subprocess.PIPE)
				assert p2.returncode is None
				lines = int(p2.stdout.read().decode(sys.stdout.encoding).strip())
			else:
				with open(fname, "r") as handle:
					lines = 0
					for line in handle:
						lines += 1		
		return int(lines/4)
			
	def check_groups(self, gztest=True, outputtest=True, fnumbertest=True, matetest=True, readcounttest=True):
		'''
		performs dataset consistency checks
		it is strongly recommended to keep all tests activated
		
		gztest			checking that all input files are gz archives
		outputtest		checking that no output file exists or serves as input file
		fnumbertest 	checking that each group contains even number of files > 2
		matetest		checking that mates can be found based on the file naming
		readcounttest	checking that paired read files contain same number of reads
		
		'''
		df = self.df
		groups = self.grouped_df
		total = len(groups)
		for label, group in groups:
			if not self.quiet:
				print("checking dataset", label, "...")	
			err = None
			r1_data = group.loc[df['strand'] == "R1"].reset_index()
			r2_data = group.loc[df['strand'] == "R2"].reset_index()
			l1 = len(r1_data.index)
			l2 = len(r2_data.index)

			#gz check
			if gztest and not err:
				for fname in df['absname'].unique().tolist():
					if not self.is_gz(fname):
						sys.exit("Error: input files must be gzip compressed and must end with .gz or .gzip")
						
			#output test
			if outputtest and not err:
				for fname in [self.name_log(label)] + self.name_fq(label):
					if os.path.isfile(fname) and not self.force:
						sys.exit("Error, file name collision: one or more designated output files exist already. Use different prefix (-p) or force overwriting (--force).")
					if fname in self.df['absname']:
						sys.exit("Error, file name collision: at least one input file will be the output file of another dataset.")
						
			#file number test
			if fnumbertest and not err:
				if l1 == 1 and l2 == 1:
					print("WARNING: only one file pair for '" + label + "*'. There is nothing to merge.", file=sys.stderr)
					self.excluded.add(label)
					err = True
				if l1 != l2:
					print("WARNING: different number of R1 and R2 files for '" + label + "*'. Respective files will be not merged.", file=sys.stderr)
					self.excluded.add(label)
					err = True

			#mate check
			if matetest and not err:
				for i in range(l1):
					if r1_data.iloc[i][['sample', 'lane', 'tile']].tolist() != r2_data.iloc[i][['sample', 'lane', 'tile']].tolist():
						print("WARNING: mate(s) missing in dataset '" + label + "*'. Respective files will be not merged.", file=sys.stderr)
						self.excluded.add(label)
						err = True
						break

			#read count check
			if readcounttest and not err:
				for i in range(l1):
					if r1_data.iloc[i]['reads'] is None:
						r1_data.iloc[i]['absname']
						self.df.loc[self.df['id'] == r1_data.iloc[i]['id'], 'reads'] = rc1 = self.count_reads(r1_data.iloc[i]['absname'])
					if r2_data.iloc[i]['reads'] is None:
						self.df.loc[self.df['id'] == r2_data.iloc[i]['id'], 'reads'] = rc2 = self.count_reads(r2_data.iloc[i]['absname'])
					if rc1 != rc2:
						print("WARNING: different number of R1 and R2 files for '" + label + "*'. Respective files will be not merged.", file=sys.stderr)
						err = True
						break
			self._grouped_df = None
			
	def merge(self, log = True):
		groups = self.grouped_df
		skipped = 0
		total = len(groups)
		succeeded = 0
		failed = 0
		for label, group in self.grouped_df:
			print("merging", total, "datasets ... [succeeded: " + str(succeeded) + ", skipped: " + str(skipped) + ", failed: " + str(failed) + "]", end = "\r")
			r1_data = group.loc[group.strand == "R1"]
			r2_data = group.loc[group.strand == "R2"]
			err = None
			log_rows = []
			if label in self.excluded:
				skipped += 1
				continue
			else:
				file_pairs = [r1_data['absname'].to_list(), r2_data['absname'].to_list()]
				file_reads = [r1_data['reads'].to_list(), r2_data['reads'].to_list()]
				dsts = self.name_fq(label)
				if self._cat:
					for i in range(2):
						srcs = file_pairs[i]			
						try:
							with open(dsts[i], "wb") as handle:
								p = subprocess.check_call(('cat', *srcs), stdout=handle)
						except subprocess.CalledProcessError:
							err = True
							for dst in dsts:
								if os.path.is_file(dst):
									os.remove(dst)
							if log:
								with open(self.name_log(label), "w") as handle:
									e = sys.exc_info()[0]
									handle.write("An error occured:\n" + e)									
							break
				else:
					for i in range(2):
						with gzip.open(dsts[i], 'wb') as outhandle:
							for sourcefile in file_pairs[i]:
								with open(sourcefile, 'rb') as inhandle:
									shutil.copyfileobj(inhandle, outhandle)
				for i, file_pair in enumerate(zip(*file_pairs)):
					log_rows.append([file_pair[0], file_pair[1], file_reads[0][i], file_reads[1][i]])
						
				if err:
					failed += 1
				else:
					succeeded += 1
					if self.remove:
						for sourcefile in file_pairs[0] + file_pairs[1]: 
							os.remove(sourcefile)
					if log:		
						with open(self.name_log(label), "w") as handle:
							handle.write("#dataset: " + label + "\n")
							handle.write("#merged using: batchMerge " + __version__ + "\n")
							handle.write("#merge date: " + datetime.date.today().ctime() + "\n")
							log_rows = [["#forward file", "reverse file", "forward reads", "reverse reads"]] + log_rows
							handle.write("\n".join(self.format_table(*log_rows, algn=["l", "l", "r", "r"])) + "\n")								
							handle.write("##SUCCESS")	
		print ("merging", total, "datasets ... [succeeded: " + str(succeeded) + ", skipped: " + str(skipped) + ", failed: " + str(failed) + "]")							
	
def main():
	parser = argparse.ArgumentParser(prog="batchMerge.py", description="dataset-specific merge of paired fastq files", )
	parser.add_argument('fqs', metavar='FILES', help="gz-compressed fastq files", type=argparse.FileType('r'), nargs="+")
	parser.add_argument('-p', metavar='STR', help="prefix used for output files (default: Merged-)", type=str, default="Merged-")
	parser.add_argument('-o', metavar='DIR', help="existing directory to store merged files", type=str, default=None)
	parser.add_argument('--force', help="force overwriting of existing files", action='store_true')
	parser.add_argument('--remove', help="remove source files after successful merge", action='store_true')
	parser.add_argument('--version', action='version', version='%(prog)s ' + __version__)
	args = parser.parse_args()	
	args.p = args.p.strip()
	if len(args.p) == 0:
		exit("ERROR: prefix (-p) has not to be empty.")
	fnames = set([x.name for x in args.fqs])
	merger = fqmerger(*fnames, prefix=args.p, force=args.force, remove=args.remove, outdir=args.o)
	if not merger._zcat and not merger._gunzip:
		print("NOTE: zcat or gunzip is missing. Switch to python-only read counting method (very slow)", file=sys.stderr)
	if not merger._cat:
		print("NOTE: cat is missing. Switch to python-only gzip writing method (very slow)", file=sys.stderr)
	merger.check_groups()
	merger.merge()
				
if __name__ == "__main__":

	pd_version = [int(x) for x in pd.__version__.split(".")]
	if pd_version[0] == 0 and pd_version[1] < 23:
		sys.exit("error: pandas version >= 0.23.0 needed. Please install the current version using following command: pip3 install pandas")
	
	main()
